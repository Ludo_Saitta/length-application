#ifndef CUSTOMLABEL_H
#define CUSTOMLABEL_H

#include <QObject>
#include <QLabel>

class CustomLabel : public QLabel
{
    Q_OBJECT //Macro

public:
    CustomLabel(const QString &text, QWidget *parent = nullptr);
    ~CustomLabel();
};

#endif // CUSTOMLABEL_H
