#ifndef CUSTOMBUTTON_H
#define CUSTOMBUTTON_H

#include <QPushButton>
#include <QObject>

class CustomButton : public QPushButton
{
    Q_OBJECT //Macro

public:
    CustomButton(const QString &text,const QString &helpText, QWidget *parent);
    ~CustomButton(); //Destructor

};

#endif // CUSTUMBUTTON_H
