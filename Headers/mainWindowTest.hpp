#ifndef MAINWINDOWTEST_H
#define MAINWINDOWTEST_H

#include <QObject>
#include <mainwindow.h>

class MainWindowTest : public MainWindow
{
public:
    MainWindowTest();
    void CreateSentenceTest();
};

#endif // MAINWINDOWTEST_H
