#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "custombutton.h"
#include "customlabel.h"
#include <QFormLayout>

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);

protected:
    void CreateSentence();
    double *len;
    QString *unit;
    QString *finalSentence;

private slots:
    void Exit();
    void SentenceDisplay();
    void Clear();
    void WriteLength();
    void ChoseUnit();

private:
    CustomLabel *Sentence;
    QGridLayout *layoutPrincipal;

    CustomButton *WriteButton;
    CustomButton *UnitButton;
    CustomButton *DisplayButton;
    CustomButton *ClearButton;
    CustomButton *ExitButton;

};
#endif // MAINWINDOW_H
