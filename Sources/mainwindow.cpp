#include "mainwindow.h"
#include "custombutton.h"
#include "customlabel.h"
#include <QWidget>
#include <QDebug>
#include <QGridLayout>
#include <QInputDialog>


MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{

    //WINDOW PARAMETERS
    setFixedSize(650,350);  //fixe la taille de la fenêtre
    setStyleSheet("background-color:#F5F5F5;"); //Background color
    setWindowIcon(QIcon(":/icons/ruller.ico")); //Icon in the top left corner of the window

    //CREATION AND INITIALISATION OF THE DEFAULT VALUES
    unit = new QString("m");
    len = new double(10.0);
    finalSentence = new QString("The length is not defined.");

    //Creation of the write button
    CustomButton *WriteButton = new CustomButton("&Set Length", "Write your length", this);
    connect(WriteButton, SIGNAL(clicked(bool)), this, SLOT(WriteLength()));

    //Creation of the choseUnit button
    CustomButton *UnitButton = new CustomButton("&Set Unit", "Choose a unit from the list", this);
    connect(UnitButton, SIGNAL(clicked(bool)), this, SLOT(ChoseUnit()));

    //Creation of the final display
    CustomButton *DisplayButton = new CustomButton("&Display", "Change the sentence with your values", this);
    connect(DisplayButton, SIGNAL(clicked(bool)), this, SLOT(SentenceDisplay()));

    //Creation of the sentence label
    Sentence = new CustomLabel("The length is not defined");

    //Creation for cleraing the display
    CustomButton *ClearButton = new CustomButton("&Clear", "Back to the original sentence", this);
    connect(ClearButton, SIGNAL(clicked(bool)), this, SLOT(Clear()));

    //Creation of the exit button
    CustomButton *ExitButton = new CustomButton("&Exit", "Close the application", this);
    connect(ExitButton, SIGNAL(clicked(bool)), this, SLOT(Exit()));

    //Placing widgets on the window
    QGridLayout *layoutPrincipal = new QGridLayout; // Main layout creation

    layoutPrincipal->addWidget(WriteButton, 0, 0); //add to the layout
    layoutPrincipal->addWidget(UnitButton, 0, 1); //add to the layout
    layoutPrincipal->addWidget(DisplayButton, 0, 2); //add to the layout
    layoutPrincipal->addWidget(Sentence, 2, 0, 1, 3); //add to the layout
    layoutPrincipal->addWidget(ClearButton, 3, 0); //add to the layout
    layoutPrincipal->addWidget(ExitButton, 3, 2); //add to the layout

    setLayout(layoutPrincipal);
    setWindowTitle(tr("Simple Length app"));
}





//Length writing (only double)
void MainWindow::WriteLength()
{
    //Creation of the dialog window to write the wished length.
    bool ok = false; //Force the user to make a choice
    *len = QInputDialog::getDouble(this, tr("Length choice"),
                                       tr("Write a length"),
                                       10.0, //Default value
                                       0, //Minimum limit --> no negative value accepted
                                       10000, //Maximum limit
                                       2, //Number of maximum decimals
                                       &ok); //Choice made --> validation of the function

}

//Length's unit
void MainWindow::ChoseUnit()
{
    bool ok = false; //Force the user to write a length
    //Creation of a list of possible units
    QStringList items; //Possibility to add units in the future to evolve the application
        items << tr("m") << tr("Km") << tr("mm") << tr("ft") << tr("in");

    //Creation of the dialog window to write or choose the wished unit.
    *unit = QInputDialog::getItem(this, tr("Unit choice"),
                                        tr("Choose the unit"),
                                        items, //List of units from which the user will have to choose
                                        0, //Default shown value
                                        //true
                                        false, //Not editable!! Put "true" to let the user write his own unit if it is not proposed in the list.
                                        &ok); //The user has to choose a unit (not very useful if inputDialog is not editable

}

//Creation of the final sentence with length and unit of the user
void MainWindow::CreateSentence()
{
    //Conacatenation of QString
    *finalSentence = QString("The length is: ") + QString::number(*len) + QString(" ") + *unit + QString(".");
}

//Display the final sentence with the length and the unit
void MainWindow::SentenceDisplay()
{
    CreateSentence(); //Sentence creation with the previous function
    Sentence->setText(*finalSentence); //Modification of the display
    Sentence->setFont(QFont("Calibri", 30)); //Font and size
}

//Display the original sentence and reset the length and unit values to the default value.
void MainWindow::Clear()
{
    *unit = "m"; //Default unit
    *len = 10.0; //Default length
    Sentence->setText("The length is not defined."); //Modification of the display
    Sentence->setFont(QFont("Calibri", 30)); //Font and size
}

//Application closing
void MainWindow::Exit()
{
    close();
}
