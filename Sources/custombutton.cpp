#include "custombutton.h"
#include <QDebug>
#include <QWidget>


CustomButton::CustomButton(const QString &text, const QString &helpText, QWidget *parent)
    : QPushButton(parent)
{
    setText(text); //Button title
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Maximum); //Space taken by the button in the grid
    setToolTip(helpText); //Help text when the mouse points on the button
    setCursor(Qt::PointingHandCursor); //Mouse replaced by a hand
    setFont(QFont("Calibri", 14)); //Font and size
    setStyleSheet("background-color: #1bb8d0;"); //Background color in hex
}

//Destructor
CustomButton::~CustomButton()
{
    //qDebug() << "Destruction Button ok" << endl;
}

