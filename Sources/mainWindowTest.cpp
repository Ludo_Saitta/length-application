#include "mainwindowtest.h"
#include <QString>
#include <QDebug>

MainWindowTest::MainWindowTest()
    :MainWindow()
{
    //CreateSentence function test
    len = new double (32.2);
    unit = new QString("in");
    finalSentence = new QString ("The concatenation does not work.");
}

//Test on QString concatenation.
//If the test works, there should be written the complete sentence
//with the length and unit entered in the test class.
//If the test does not work, the sentence "The concatenation does not work"
//appears. The result of the test is in the console after the main window is closed.

void MainWindowTest::CreateSentenceTest()
{
    CreateSentence(); //Use of the super class method
    qDebug() << *finalSentence; //Result written in the console of Qt creator
}
