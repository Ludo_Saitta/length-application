#include "customlabel.h"
#include <QWidget>

CustomLabel::CustomLabel(const QString &text, QWidget *parent)
    : QLabel(parent)
{
    setText(text); //Message writen in the label
    setFrameStyle(QFrame::StyledPanel | QFrame::Sunken); //Label outlines
    setAlignment(Qt::AlignCenter); //Centered text
    setFont(QFont("Calibri", 30)); //Font and size
    setStyleSheet("background-color:#1bb8d0;"); //Background color
    setFixedSize(628,220); //Position in the window
}

CustomLabel::~CustomLabel()
{

}
