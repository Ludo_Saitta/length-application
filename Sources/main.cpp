#include "mainwindow.h"
#include "mainwindowtest.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    //Main window
    MainWindow w;
    w.show();
    a.exec();

    //UNIT TEST
    MainWindowTest wtest;
    wtest.CreateSentenceTest();

    return 0;
}
